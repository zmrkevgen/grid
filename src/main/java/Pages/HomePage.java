package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage {
    public HomePage(WebDriver driver) {
        super(driver);
    }
    public static final String LOGO_CHECKER = "//div[@class='header-left']";
    public static final String CONTENT_CHECKER = "//section[@class='content']";

    public WebElement getLogo_checker(){
        return logo_checker;
    }
    public WebElement getContent_checker(){
        return content_checker;
    }

    @FindBy(xpath = LOGO_CHECKER)
    private WebElement logo_checker;

    @FindBy(xpath = CONTENT_CHECKER)
    private WebElement content_checker;
}
