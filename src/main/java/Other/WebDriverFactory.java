package Other;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class WebDriverFactory {
    public static WebDriver getDriver(String name) {
        WebDriver driver = null;
        if ("ch".equalsIgnoreCase(name)) {
            System.setProperty("webdriver.chrome.driver", "D://Java for AQA//uafootball//src//main//resources//chromedriver.exe");
            driver = new ChromeDriver();
        }
        else if("ff".equalsIgnoreCase(name)){
            System.setProperty("webdriver.gecko.driver", "D://Java for AQA//uafootball//src//main//resources//geckodriver.exe");
            driver = new FirefoxDriver();
        }
        else if ("ie".equalsIgnoreCase(name)){
            System.setProperty("webdriver.edge.driver", "D://Java for AQA//uafootball//src//main//resources//MicrosoftWebDriver.exe");
        }

        return driver;
    }
}
