package Other;

import org.apache.log4j.LogManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.WebDriverEventListener;

import static java.lang.System.out;

public class Logger implements WebDriverEventListener {

    public static final org.apache.log4j.Logger LOG = LogManager.getLogger(Logger.class);


    @Override
    public void beforeAlertAccept(WebDriver webDriver) {
        LOG.info("Now you alert will be accept");
    }

    @Override
    public void afterAlertAccept(WebDriver webDriver) {
        LOG.info("You alert accepted");
    }

    @Override
    public void afterAlertDismiss(WebDriver webDriver) {
        out.println("You alert dismissed");
    }

    @Override
    public void beforeAlertDismiss(WebDriver webDriver) {
        out.println("You alert will be dismiss");
    }

    @Override
    public void beforeNavigateTo(String s, WebDriver webDriver) {
        LOG.info("You navigate to the page");
    }

    @Override
    public void afterNavigateTo(String s, WebDriver webDriver) {
        LOG.info("You successfully navigate");
    }

    @Override
    public void beforeNavigateBack(WebDriver webDriver) {
        LOG.info("You navigate on the previous page");
    }

    @Override
    public void afterNavigateBack(WebDriver webDriver) {
        LOG.info("You have navigated on the previous page");
    }

    @Override
    public void beforeNavigateForward(WebDriver webDriver) {
        LOG.info("You navigate on the forward page");
    }

    @Override
    public void afterNavigateForward(WebDriver webDriver) {
        LOG.info("You have navigated on the forward page");
    }

    @Override
    public void beforeNavigateRefresh(WebDriver webDriver) {
        LOG.info("You page will be refresh");
    }

    @Override
    public void afterNavigateRefresh(WebDriver webDriver) {
        LOG.info("You page was successfully refreshed");
    }

    @Override
    public void beforeFindBy(By by, WebElement webElement, WebDriver webDriver) {
        LOG.info("You will find " +  by);
    }

    @Override
    public void afterFindBy(By by, WebElement webElement, WebDriver webDriver) {
        LOG.info("You have successfully found " +  by);
    }

    @Override
    public void beforeClickOn(WebElement element, WebDriver driver) {
        LOG.info("We will click on the element " + element);
    }

    @Override
    public void afterClickOn(WebElement webElement, WebDriver webDriver) {
        LOG.info("Clicked successful ");
    }

    @Override
    public void beforeChangeValueOf(WebElement webElement, WebDriver webDriver, CharSequence[] charSequences) {
        LOG.info("We will change next element " + webElement);
    }

    @Override
    public void afterChangeValueOf(WebElement webElement, WebDriver webDriver, CharSequence[] charSequences) {
        LOG.info("We have successfully changed next element " + webElement);
    }

    @Override
    public void beforeScript(String s, WebDriver webDriver) {
        LOG.info("Now we will start this perfect autotest ");
    }

    @Override
    public void afterScript(String s, WebDriver webDriver) {
        LOG.info("It is all. We have finish! Congratulation! ");
    }

    @Override
    public void onException(Throwable throwable, WebDriver webDriver) {
        out.println("It is a success ");
    }
}