import Pages.HomePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.BeforeTest;

import java.net.MalformedURLException;
import java.net.URL;

import static Helpers.BaseHelper.openApp;
import static Other.Constants.HOME_URL;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class Test {

    @BeforeTest
    public void projectSettings() {
        System.setProperty("webdriver.chrome.driver", "D://Java for AQA//uafootball//src//main//resources//chromedriver.exe");

    }


    @org.testng.annotations.Test
    private void firstTest() throws MalformedURLException {
        WebDriver driver = new RemoteWebDriver(new URL("http://localhost:4445/wd/hub"), new ChromeOptions());

        HomePage homePage = new HomePage(driver);
        openApp(driver,HOME_URL);
        assertTrue(homePage.getLogo_checker().isDisplayed());

        driver.close();
    }
    @org.testng.annotations.Test
    private void secondTest()throws MalformedURLException{
        WebDriver driver = new RemoteWebDriver(new URL("http://localhost:4445/wd/hub"), new ChromeOptions());
        HomePage homePage = new HomePage(driver);
        openApp(driver,HOME_URL);
        assertTrue(homePage.getContent_checker().isDisplayed());

    }
}
